'''
Created on 28 nov. 2020

@author: Rafael Villaneda
'''
from random import *
import time
class MetodosOrdenamientos:
    def __init__(self):
        self.contador=[0,0,0]
    def mostrarContador(self):
        #1-> recorridos 2-> intercambios 3-> comparaciones
        print(f"Numeros de recorridos: {self.contador[0]}")
        print(f"Numeros de intercambios: {self.contador[1]}")
        print(f"Numeros de Comparaciones: {self.contador[2]}")
        self.contador[0]=0
        self.contador[1]=0
        self.contador[2]=0
    def  ordenacionBurbuja1(self,arreglo):
        for i in range(1, len(arreglo)):
            for j in range(0, len(arreglo)-i):
                self.contador[2]=self.contador[2]+1
                if arreglo[j]>arreglo[j+1]:
                    aux = arreglo[j]
                    self.contador[1]= self.contador[1]+1
                    arreglo[j] = arreglo[j+1]
                    self.contador[1]= self.contador[1]+1
                    arreglo[j+1] = aux
                self.contador[0]=self.contador[0]+1
            self.contador[0]=self.contador[0]+1

    def  ordenacionBurbuja3(self,arreglo):
        cont = 1
        inicio = time.time()
        while cont<len(arreglo):
            self.contador[0]=self.contador[0]+1
            for j in range(0, len(arreglo)-cont):
                self.contador[0]=self.contador[0]+1
                if arreglo[j]>arreglo[j+1]:
                    self.contador[2]=self.contador[2]+1
                    self.contador[1]=self.contador[1]+1
                    aux = arreglo[j]
                    arreglo[j] = arreglo[j+1]
                    self.contador[1]=self.contador[1]+1
                    arreglo[j+1] = aux
            cont+=1
    def  ordenacionBurbuja2(self,arreglo):
        i = 0
        while i<len(arreglo):
            j=i
            while j<len(arreglo):
                self.contador[2]=self.contador[2]+1
                if arreglo[i]>arreglo[j]:
                    aux = arreglo[i]
                    self.contador[1]=self.contador[1]+1
                    arreglo[i] = arreglo[j]
                    self.contador[1]=self.contador[1]+1
                    arreglo[j] = aux
                self.contador[0]=self.contador[0]+1
                j+=1
            self.contador[0]=self.contador[0]+1
            i+=1
    def ordenarInsercion(self,numeros):
        aux=0
        for i in range(1,len(numeros)):
            aux=numeros[i]
            j=(i-1)
            while(j>=0 and numeros[j]>aux):
                self.contador[2]=self.contador[2]+1
                self.contador[1]=self.contador[1]+1
                numeros[j+1]=numeros[j]
                self.contador[1]=self.contador[1]+1
                numeros[j]=aux
                j-=1
                self.contador[0]=self.contador[0]+1
            self.contador[0]=self.contador[0]+1
    def ordenarSeleccion(self,numeros):
        #1-> recorridos 2-> intercambios 3-> comparaciones
        for i in range(0,len(numeros)):
            for j in range(i,len(numeros)):
                self.contador[2]=self.contador[2]+1
                if(numeros[i]>numeros[j]):
                    minimo=numeros[i]
                    self.contador[1]=self.contador[1]+1
                    numeros[i]=numeros[j]
                    self.contador[1]=self.contador[1]+1
                    numeros[j]=minimo
                self.contador[0]=self.contador[0]+1
            self.contador[0]=self.contador[0]+1
    def ordenarQuickSort(self,numeros,izq,der):
        pivote=numeros[izq]
        i=izq
        j=der
        aux=0
        while i < j:
            while numeros[i]<= pivote and i<j:
                i+=1
                self.contador[0]=self.contador[0]+1
            while numeros[j]>pivote:
                j-=1
                self.contador[0]=self.contador[0]+1
            if(i<j):
                aux=numeros[i]
                self.contador[1]=self.contador[1]+1
                numeros[i]=numeros[j]
                self.contador[1]=self.contador[1]+1
                numeros[j]=aux
            self.contador[0]=self.contador[0]+1
        self.contador[1]=self.contador[1]+1
        numeros[izq]=numeros[j]
        self.contador[1]=self.contador[1]+1
        numeros[j]=pivote
        self.contador[2]=self.contador[2]+1
        if izq<j-1:
            self.ordenarQuickSort(numeros, izq, j-1)
        self.contador[2]=self.contador[2]+1
        if j+1<der:
            self.ordenarQuickSort(numeros, j+1, der)
    def ordenarShellsort(self,numeros):
        intervalo=len(numeros)/2
        intervalo=int(intervalo)
        while(intervalo>0):
            for i in range(int(intervalo),len(numeros)):
                j=i-int(intervalo)
                while(j>=0):
                    k=j+int(intervalo)
                    self.contador[2]=self.contador[2]+1
                    if(numeros[j]<=numeros[k]):
                        j=-1
                    else:
                        aux=numeros[j]
                        self.contador[1]=self.contador[1]+1
                        numeros[j]=numeros[k]
                        self.contador[1]=self.contador[1]+1
                        numeros[k]=aux
                        j-=int(intervalo)
                    self.contador[0]=self.contador[0]+1
                self.contador[0]=self.contador[0]+1
            intervalo=int(intervalo)/2
            self.contador[0]=self.contador[0]+1

    def ordenar(self,numeros,exp1):
        n=len(numeros)
        cantidadDatos=[0]*(n)
        contar=[0]*(10)
        
        for i in range(0,n):
            indice=(numeros[i]/exp1)
            contar[int((indice)%10)]+=1
            self.contador[0]=self.contador[0]+1
        for i in range(1,10):
            contar[i]+=contar[i-1]
            self.contador[0]=self.contador[0]+1
        i=n-1
        while i>=0:
            self.contador[2]=self.contador[2]+1
            indice=(numeros[i]/exp1)
            cantidadDatos[contar[int((indice)%10)]-1]=numeros[i]
            contar[int((indice)%10)]-=1
            i-=1
            self.contador[0]=self.contador[0]+1
        i=0
        
        for i in range(0,len(numeros)):
            numeros[i]=cantidadDatos[i]
            self.contador[0]=self.contador[0]+1
    
    def radixSort(self,numeros):
        max1=max(numeros)
        exp=1
        while max1/exp>0:
            self.contador[2]=self.contador[2]+1
            self.ordenar(numeros,exp)
            exp*=10
            self.contador[0]=self.contador[0]+1
    

class PruebasEstres:
    def pruebaEstres(self,op):
        vector1000E=[randint(0,1000) for i in range(1000)]
        vector10000E=[randint(0,1000) for i in range(10000)]
        vector100000E=[randint(0,1000) for i in range(100000)]
        vector1=[10,12,5,2,8,3,1,7,22,6,30,4,11,2,9]
        if(op=="1"):
            self.pruebas(vector1)
        elif(op=="2"):
            self.pruebas(vector1000E)
        elif(op=="3"):
            self.pruebas(vector10000E)
        elif(op=="4"):
            self.pruebas(vector100000E)
        else:
            print("Elejiste una opcion incorrecta")
        pass
    def pruebas(self,vector):
        ordenar=MetodosOrdenamientos()
        tiempoIn=0
        print("--------------------Burbuja 1 ----------------------------------")
        copi=vector.copy()
        print(copi)
        tiempoIn=time.time()
        ordenar.ordenacionBurbuja1(copi)
        tFin=time.time()
        print(f"Tardo: {(tFin-tiempoIn)/1000}")
        ordenar.mostrarContador()
        tiempoIn=tFin=0
        copi.clear()
        copi=vector.copy()
        print("--------------------Burbuja 2 ----------------------------------")
        tiempoIn=tFin=0
        tiempoIn=time.time()
        ordenar.ordenacionBurbuja2(copi)
        tFin=time.time()
        print(f"Tardo: {(tFin-tiempoIn)/1000}")
        ordenar.mostrarContador()
        copi.clear()
        copi=vector.copy()
        print("--------------------Burbuja 3 ----------------------------------")
        tiempoIn=tFin=0
        tiempoIn=time.time()
        ordenar.ordenacionBurbuja3(copi)
        tFin=time.time()
        print(f"Tardo: {(tFin-tiempoIn)/1000}")
        ordenar.mostrarContador()
        copi.clear()
        copi=vector.copy()
        print("-------------------- Insercion ----------------------------------")
        tiempoIn=tFin=0
        tiempoIn=time.time()
        ordenar.ordenarInsercion(copi)
        tFin=time.time()
        print(f"Tardo: {(tFin-tiempoIn)/1000}")
        ordenar.mostrarContador()
        copi.clear()
        copi=vector.copy()
        print("-------------------- Seleccion ----------------------------------")
        tiempoIn=tFin=0
        tiempoIn=time.time()
        ordenar.ordenarSeleccion(copi)
        tFin=time.time()
        print(f"Tardo: {(tFin-tiempoIn)/1000}")
        ordenar.mostrarContador()
        copi.clear()
        copi=vector.copy()
        print("-------------------- QuickSort ----------------------------------")
        tiempoIn=tFin=0
        tiempoIn=time.time()
        ordenar.ordenarQuickSort(copi,0,len(copi)-1)
        tFin=time.time()
        print(f"Tardo: {(tFin-tiempoIn)/1000}")
        ordenar.mostrarContador()
        copi.clear()
        copi=vector.copy()
        print("-------------------- Shellsort ----------------------------------")
        tiempoIn=tFin=0
        tiempoIn=time.time()
        ordenar.ordenarShellsort(copi)
        tFin=time.time()
        print(f"Tardo: {(tFin-tiempoIn)/1000}")
        ordenar.mostrarContador()
        copi.clear()
        copi=vector.copy()
        print("-------------------- radixSort ----------------------------------")
        tiempoIn=tFin=0
        tiempoIn=time.time()
        ordenar.radixSort(copi)
        tFin=time.time()
        print(f"Tardo: {(tFin-tiempoIn)/1000}")
        ordenar.mostrarContador()
        copi.clear()
        copi=vector.copy()
        print("-------------------------------------------------------------------------------")
        pass
    

bandera=False
prueba=PruebasEstres()
while (bandera==False):
    print("Con que quieres probar?")
    print("1-> Ordenar elementos precargados")
    print("2-> Ordenar 1000 elementos")
    print("3-> Ordenar 10000 elementos")
    print("4-> Ordenar 100000 elementos")
    print("5-> Salir")
    op=input()
    if(op=="1"):
        prueba.pruebaEstres(op)
    elif(op=="2"):
        prueba.pruebaEstres(op)
    elif(op=="3"):
        prueba.pruebaEstres(op)
    elif(op=="4"):
        prueba.pruebaEstres(op)
    elif(op=="5"):
        print("Saliendo....")
        bandera=True